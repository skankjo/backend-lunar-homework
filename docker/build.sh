cp ../build/libs/backend-lunar-homework-0.0.1-SNAPSHOT.jar ./files
sudo docker pull alpine
sudo docker build --tag=alpine-java:base --rm=true .
sudo docker build --file=Dockerfile.server --tag=lunar-server:latest --rm=true .
sudo docker run --name=lunar-server --publish=8080:8080 lunar-server:latest
