#!/bin/bash

sudo apt -y install parallel

function crawl() {
url=$1
	curl http://localhost:8080/ -H "Content-Type: application/json"
	curl http://localhost:8080/job -X POST -d "{\"url\":\"$url\"}" -H "Content-Type: application/json"
	curl http://localhost:8080/job?url=$url -X PUT  -d "{\"famousPeople\": [\"Jacky Chan\", \"Michael Jackson\", \"Madonna\", \"Caesar\"]}" -H "Content-Type: application/json"
	curl http://localhost:8080/job/search?url=$url -H "Content-Type: application/json"
}

export -f crawl

parallel -a random_urls crawl
