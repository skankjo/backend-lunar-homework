# backend-lunar-homework
It's a backend homework for lunar
## Prerequisites
- [java](https://www.oracle.com/java/index.html) (at least 8 version)
- [docker](https://www.docker.com)
## Building and running
```shell
git clone https://skankjo@bitbucket.org/skankjo/backend-lunar-homework.git
./build.sh
```
It will package the project itself, install it into docker container and run it in it.
If you want to run it locally, just run:
```shell
./run.sh
```
In both cases the project is accessible by [http://localhost:8080](http://localhost:8080)
## Running tests
```shell
./gradlew test
```
Or you can run crawlers in parallel with this command:
```shell
cd docker/files
./crawler.sh
```
## Api Documentation
Api Documentation is accessible by [http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html)
## Health status
Health status is at this url: [http://localhost:8080/health](http://localhost:8080/health)
## Metrics
Metrics are accessible by: [http://localhost:8080/metrics](http://localhost:8080/metrics)
