package lt.skankjo.lunar.backendhomework.transformers;

import lt.skankjo.lunar.backendhomework.model.entities.Celebrity;
import lt.skankjo.lunar.backendhomework.model.entities.Job;
import lt.skankjo.lunar.backendhomework.rest.resources.job.JobPutRequest;
import lt.skankjo.lunar.backendhomework.rest.resources.job.JobResource;
import lt.skankjo.lunar.backendhomework.rest.resources.job.JobResponse;
import lt.skankjo.lunar.backendhomework.rest.resources.job.UnfinishedJobsResponse;
import org.springframework.hateoas.Link;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

public class JobResponseTransformer {

    public static JobResponse transform(Job job) {
        JobResponse response = new JobResponse(job.getUrl(), job.getKey(), job.getStatus().name());
        response.add(buildJobSelfLink(job.getUrl()));
        response.add(buildJobEditLink(job.getUrl()));
        response.add(buildJobFinishLink(job.getUrl(), job.getKey()));
        response.setFamousPeople(buildFamousPeople(job.getFamousPeople()));
        return response;
    }

    public static UnfinishedJobsResponse transform(List<Job> jobs) {
        List<JobResponse> jobResponses = jobs.stream()
                .map(JobResponseTransformer::transform)
                .collect(Collectors.toList());
        UnfinishedJobsResponse unfinishedJobsResponse = new UnfinishedJobsResponse(jobResponses);
        unfinishedJobsResponse.add(buildJobListSelfLink());
        return unfinishedJobsResponse;
    }

    private static Link buildJobSelfLink(String url) {
        return linkTo(methodOn(JobResource.class).search(url)).withSelfRel();
    }

    private static Link buildJobEditLink(String url) {
        return linkTo(methodOn(JobResource.class).edit(url, new JobPutRequest())).withRel("edit");
    }

    private static Link buildJobFinishLink(String url, String key) {
        return linkTo(methodOn(JobResource.class).finish(url, key)).withRel("finish");
    }

    private static Link buildJobListSelfLink() {
        return linkTo(methodOn(JobResource.class).list()).withSelfRel();
    }

    private static List<String> buildFamousPeople(List<Celebrity> famousPeople) {
        return Optional.ofNullable(famousPeople)
                .orElse(Collections.emptyList())
                .stream()
                .map(celebrity -> celebrity.getName())
                .collect(Collectors.toList());
    }
}
