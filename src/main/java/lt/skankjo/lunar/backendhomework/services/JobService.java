package lt.skankjo.lunar.backendhomework.services;

import lt.skankjo.lunar.backendhomework.model.entities.Celebrity;
import lt.skankjo.lunar.backendhomework.model.entities.Job;
import lt.skankjo.lunar.backendhomework.model.repositories.JobRepository;
import lt.skankjo.lunar.backendhomework.rest.exceptions.AlreadyExistsException;
import lt.skankjo.lunar.backendhomework.rest.exceptions.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static lt.skankjo.lunar.backendhomework.model.entities.Job.Status.FINISHED;
import static lt.skankjo.lunar.backendhomework.model.entities.Job.Status.INITIATED;


@Service
public class JobService {

    private final JobRepository jobRepository;

    @Autowired
    public JobService(JobRepository jobRepository) {
        this.jobRepository = jobRepository;
    }

    @Transactional()
    public Job save(Job job) {
        Optional<Job> maybe = jobRepository.findByUrl(job.getUrl());
        if (maybe.isPresent()) {
            throw new AlreadyExistsException("This url exists already");
        }
        return jobRepository.save(job);
    }

    @Transactional(readOnly = true)
    public Job getByUrl(String url) {
        Optional<Job> job = jobRepository.findByUrl(url);
        return job.orElseThrow(() -> new NotFoundException("No job by url specified"));
    }

    @Transactional
    public Job finish(String url) {
        Optional<Job> job = jobRepository.findByUrl(url);
        return job.map(item -> {
            item.setStatus(FINISHED);
            return jobRepository.save(item);
        }).orElseThrow(() -> new NotFoundException("No job by url specified"));
    }

    @Transactional
    public Job addFamousPeople(Job job, List<Celebrity> famousPeople) {
        job.setFamousPeople(famousPeople);
        job.setStatus(FINISHED);
        return jobRepository.save(job);
    }

    @Transactional(readOnly = true)
    public List<Job> listUnfinished() {
        return jobRepository.findByStatus(INITIATED);
    }
}
