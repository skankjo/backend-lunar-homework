package lt.skankjo.lunar.backendhomework.validators;

public interface Validator<T> {
    void validate(T validatee);
}
