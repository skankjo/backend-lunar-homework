package lt.skankjo.lunar.backendhomework.validators;

import javax.validation.ValidationException;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class RepositoryKeyValidator implements Validator<String> {

    private static final int length = 36;

    private static final List<Function<String, Boolean>> validators = Arrays.asList(
            k -> k != null,
            s -> s.length() == length
    );

    @Override
    public void validate(String validatee) {
        ValidationException ex = new ValidationException("Wrong format of repository key");
        validators.stream()
                .map(validator -> !validator.apply(validatee))
                .filter(Boolean::booleanValue)
                .findFirst()
                .ifPresent((bool) -> { throw ex; });
    }
}
