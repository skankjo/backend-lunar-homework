package lt.skankjo.lunar.backendhomework.validators;

import lt.skankjo.lunar.backendhomework.model.entities.Job;
import lt.skankjo.lunar.backendhomework.rest.exceptions.RestClientException;
import org.springframework.http.HttpStatus;

public class JobStatusValidator implements Validator<Job> {
    @Override
    public void validate(Job validatee) {
        if (validatee.getStatus() == Job.Status.FINISHED) {
            throw new RestClientException("Job is finished. No celebrities can be added any more.", HttpStatus.BAD_REQUEST);
        }
    }
}
