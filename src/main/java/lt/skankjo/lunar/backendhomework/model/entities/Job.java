package lt.skankjo.lunar.backendhomework.model.entities;

import javax.persistence.*;

import java.util.List;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.EnumType.STRING;
import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.IDENTITY;
import static javax.persistence.GenerationType.SEQUENCE;

@Entity
public class Job {
    @Id
    @GeneratedValue(generator = "JOB_SEQ", strategy = SEQUENCE)
    @SequenceGenerator(name = "JOB_SEQ", sequenceName = "JOB_SEQ", allocationSize = 1)
    private Integer id;

    @Column(nullable = false, unique = true)
    private String url;

    @Enumerated(STRING)
    private Status status;

    @Column(nullable = false, unique = true)
    private String key;

    @OneToMany(mappedBy = "job", cascade = ALL, fetch = LAZY)
    private List<Celebrity> famousPeople;

    private Job() {}

    public Job(String url, String key, Status status) {
        this.url = url;
        this.key = key;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public List<Celebrity> getFamousPeople() {
        return famousPeople;
    }

    public void setFamousPeople(List<Celebrity> famousPeople) {
        this.famousPeople = famousPeople;
    }

    public enum Status {
        INITIATED,
        FINISHED
    }
}
