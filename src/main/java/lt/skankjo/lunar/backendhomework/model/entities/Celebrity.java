package lt.skankjo.lunar.backendhomework.model.entities;

import javax.persistence.*;

import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.SEQUENCE;

@Entity
public class Celebrity {
    @Id
    @GeneratedValue(generator = "CELEBRITY_SEQ", strategy = SEQUENCE)
    @SequenceGenerator(name = "CELEBRITY_SEQ", sequenceName = "CELEBRITY_SEQ", allocationSize = 1)
    private Integer id;

    @Column(nullable = false)
    private String name;

    @ManyToOne(optional = false, fetch = LAZY)
    @JoinColumn(name = "job_id")
    private Job job;

    private Celebrity() {}

    public Celebrity(String name, Job job) {
        this.name = name;
        this.job = job;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }
}
