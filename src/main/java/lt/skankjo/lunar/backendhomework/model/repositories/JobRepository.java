package lt.skankjo.lunar.backendhomework.model.repositories;

import lt.skankjo.lunar.backendhomework.model.entities.Job;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface JobRepository extends CrudRepository<Job, Integer> {
    Optional<Job> findByKey(String key);
    Optional<Job> findByUrl(String url);
    List<Job> findByStatus(Job.Status status);
}
