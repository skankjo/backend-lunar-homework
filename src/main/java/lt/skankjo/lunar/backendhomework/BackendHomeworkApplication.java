package lt.skankjo.lunar.backendhomework;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendHomeworkApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackendHomeworkApplication.class, args);
	}
}
