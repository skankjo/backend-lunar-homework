package lt.skankjo.lunar.backendhomework.rest.resources.job;

import lt.skankjo.lunar.backendhomework.model.entities.Celebrity;
import lt.skankjo.lunar.backendhomework.model.entities.Job;
import lt.skankjo.lunar.backendhomework.validators.JobStatusValidator;
import lt.skankjo.lunar.backendhomework.validators.RepositoryKeyValidator;
import lt.skankjo.lunar.backendhomework.services.JobService;
import lt.skankjo.lunar.backendhomework.transformers.JobResponseTransformer;
import lt.skankjo.lunar.backendhomework.transformers.JobTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("/job")
public class JobResource {

    private final JobService jobService;

    @Autowired
    public JobResource(JobService jobService) {
        this.jobService = jobService;
    }

    @PostMapping
    public HttpEntity<JobResponse> create(@RequestBody @Valid JobRequest request) {

        Job job = JobTransformer.transform(request);
        job = jobService.save(job);

        JobResponse response = JobResponseTransformer.transform(job);

        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PutMapping
    public HttpEntity<JobResponse> edit(@RequestParam("url") String url,
                                        @RequestBody @Valid JobPutRequest request) {
        final Job job = jobService.getByUrl(url);
        new JobStatusValidator().validate(job);
        List<Celebrity> famousPeople = JobTransformer.transform(request, job);
        Job saved = jobService.addFamousPeople(job, famousPeople);

        JobResponse response = JobResponseTransformer.transform(saved);
        response.setFamousPeople(request.getFamousPeople());

        return new ResponseEntity<>(response, OK);
    }

    @PutMapping(path = "/{key}")
    public HttpEntity<JobResponse> finish(@RequestParam("url") String url,
                                          @PathVariable("key") String key) {

        RepositoryKeyValidator validator = new RepositoryKeyValidator();
        validator.validate(key);

        Job job = jobService.finish(url);
        JobResponse response = JobResponseTransformer.transform(job);

        return new ResponseEntity<>(response, OK);
    }

    @GetMapping(path = "/search")
    public HttpEntity<JobResponse> search(@RequestParam("url") String url) {
        Job job = jobService.getByUrl(url);
        JobResponse response = JobResponseTransformer.transform(job);

        return new ResponseEntity<>(response, OK);
    }

    @GetMapping
    public HttpEntity<UnfinishedJobsResponse> list() {
        List<Job> jobs = jobService.listUnfinished();
        UnfinishedJobsResponse response = JobResponseTransformer.transform(jobs);

        return new ResponseEntity<>(response, OK);
    }


}
