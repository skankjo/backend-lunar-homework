package lt.skankjo.lunar.backendhomework.rest.resources.root;

import lt.skankjo.lunar.backendhomework.rest.resources.job.JobResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("/")
public class RootResource {

    @GetMapping
    public ResponseEntity<Root> get() {
        Root root = new Root();
        root.add(linkTo(methodOn(JobResource.class).list()).withRel("jobs"));
        return new ResponseEntity<>(root, OK);
    }
}
