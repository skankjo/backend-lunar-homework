package lt.skankjo.lunar.backendhomework.rest.exceptions;

import org.springframework.http.HttpStatus;

public class AlreadyExistsException extends RestClientException {
    public AlreadyExistsException(String message) {
        super(message, HttpStatus.CONFLICT);
    }
}
