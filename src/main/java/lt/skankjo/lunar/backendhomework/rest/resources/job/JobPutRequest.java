package lt.skankjo.lunar.backendhomework.rest.resources.job;

import org.hibernate.validator.constraints.NotEmpty;

import java.util.List;

public class JobPutRequest {

    @NotEmpty
    private List<String> famousPeople;

    public JobPutRequest() {}

    public List<String> getFamousPeople() {
        return famousPeople;
    }

    public void setFamousPeople(List<String> famousPeople) {
        this.famousPeople = famousPeople;
    }
}
