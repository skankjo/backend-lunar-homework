package lt.skankjo.lunar.backendhomework.rest.exceptions;

public class RestError {
    private String template;
    private String message;
    private String path;
    private int status;
    private long timestamp;

    private RestError() {}

    public RestError(String template, String message, String path, int status, long timestamp) {
        this.template = template;
        this.message = message;
        this.path = path;
        this.status = status;
        this.timestamp = timestamp;
    }

    public String getTemplate() {
        return template;
    }

    public String getMessage() {
        return message;
    }

    public String getPath() {
        return path;
    }

    public int getStatus() {
        return status;
    }

    public long getTimestamp() {
        return timestamp;
    }
}
