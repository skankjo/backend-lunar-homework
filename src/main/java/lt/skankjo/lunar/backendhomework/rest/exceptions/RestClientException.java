package lt.skankjo.lunar.backendhomework.rest.exceptions;

import org.springframework.http.HttpStatus;

public class RestClientException extends RuntimeException {
    private final HttpStatus status;

    public RestClientException(String message, HttpStatus status) {
        super(message);
        this.status = status;
    }

    public HttpStatus getStatus() {
        return status;
    }
}
