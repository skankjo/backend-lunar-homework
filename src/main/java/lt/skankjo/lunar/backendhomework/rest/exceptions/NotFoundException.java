package lt.skankjo.lunar.backendhomework.rest.exceptions;

import org.springframework.http.HttpStatus;

public class NotFoundException extends RestClientException {

    public NotFoundException(String message) {
        super(message, HttpStatus.NOT_FOUND);
    }

}
