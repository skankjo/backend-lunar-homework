package lt.skankjo.lunar.backendhomework.rest.exceptions;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ValidationException;
import java.util.Date;


@ControllerAdvice
public class RestClientExceptionHandler {

    @ExceptionHandler(ValidationException.class)
    public ResponseEntity<Object> handleValidationException(ValidationException ex, WebRequest request) {
        RestError error = new RestError(ex.getMessage(), HttpStatus.BAD_REQUEST.getReasonPhrase(), getRequestUri(request), HttpStatus.BAD_REQUEST.value(), new Date().getTime());
        return handleExceptionInternal(ex, error, null, HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(RestClientException.class)
    public ResponseEntity<Object> handleNotFoundException(RestClientException ex, WebRequest request) {
        RestError error = new RestError(ex.getMessage(), ex.getStatus().getReasonPhrase(), getRequestUri(request), ex.getStatus().value(), new Date().getTime());
        return handleExceptionInternal(ex, error, null, ex.getStatus(), request);
    }

    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return new ResponseEntity<Object>(body, headers, status);
    }

    private String getRequestUri(WebRequest request) {
        HttpServletRequest httpServletRequest = (HttpServletRequest) ((ServletWebRequest) request).getNativeRequest();
        return httpServletRequest.getRequestURI();
    }

}
