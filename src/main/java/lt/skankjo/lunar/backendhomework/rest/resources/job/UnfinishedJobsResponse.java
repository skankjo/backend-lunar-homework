package lt.skankjo.lunar.backendhomework.rest.resources.job;

import org.springframework.hateoas.ResourceSupport;

import java.util.ArrayList;
import java.util.List;

public class UnfinishedJobsResponse extends ResourceSupport {
    private List<JobResponse> jobs = new ArrayList<>();

    private UnfinishedJobsResponse() {}

    public UnfinishedJobsResponse(List<JobResponse> jobs) {
        this.jobs = jobs;
    }

    public List<JobResponse> getJobs() {
        return jobs;
    }

    public void setJobs(List<JobResponse> jobs) {
        this.jobs = jobs;
    }
}
