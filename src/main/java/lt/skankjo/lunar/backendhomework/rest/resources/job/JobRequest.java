package lt.skankjo.lunar.backendhomework.rest.resources.job;

import org.hibernate.validator.constraints.NotEmpty;

public class JobRequest {

    @NotEmpty
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
