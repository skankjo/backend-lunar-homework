package lt.skankjo.lunar.backendhomework.integration

import lt.skankjo.lunar.backendhomework.rest.exceptions.RestError
import lt.skankjo.lunar.backendhomework.rest.resources.job.JobResponse
import lt.skankjo.lunar.backendhomework.rest.resources.job.UnfinishedJobsResponse
import lt.skankjo.lunar.backendhomework.rest.resources.root.Root
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.embedded.LocalServerPort
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT

@SpringBootTest(webEnvironment = RANDOM_PORT)
class BackendHomeworkApplicationTests extends Specification {

    @Autowired
    private RestClient restClient

    @LocalServerPort
    int port

    def 'should return root entity'() {
        when:
            Root root = restClient.root
        then:
            root.hasLink('jobs')
    }

    def 'should create a job'() {
        String requestUrl = 'www.skankjo.com'
        when:
            JobResponse response = restClient.createJob(requestUrl)
        then:
            response.with {
                url == requestUrl
                status == 'INITIATED'
                famousPeople.empty
                getLink('self').href == "$domain/job/search?url=$requestUrl"
            }
    }

    def 'should add famous people to the job'() {
        String requestUrl = 'www.google.com'
        when:
            JobResponse creationResponse = restClient.createJob(requestUrl)
        and:
            JobResponse response = restClient.updateJob(creationResponse.getLink('self').href, ['Adam', 'Eva'])
        then:
            response.with {
                url == requestUrl
                status == 'FINISHED'
                famousPeople == ['Adam', 'Eva']
                getLink('self').href == "$domain/job/search?url=$requestUrl"
            }
    }

    def 'should finish the current job'() {
        String requestUrl = 'www.yahoo.com'
        when:
            JobResponse creationResponse = restClient.createJob(requestUrl)
        and:
            JobResponse response = restClient.finishJob(creationResponse.getLink('self').href)
        then:
            response.with {
                url == requestUrl
                status == 'FINISHED'
                famousPeople.empty
                getLink('self').href == "$domain/job/search?url=$requestUrl"
            }
    }

    def 'should return a list of unfinished jobs'() {
        String requestUrl = 'www.facebook.com'
        when:
            JobResponse creationResponse = restClient.createJob(requestUrl)
        and:
            UnfinishedJobsResponse response = restClient.getUnfinishedJobs()
        then:
            response.jobs.size() == 2
            response.jobs[1].with {
                url == requestUrl
                status == 'INITIATED'
                famousPeople.empty
                getLink('self').href == "$domain/job/search?url=$requestUrl"
            }
    }

    def 'should return not found status'() {
        String requestUrl = 'www.twitter.com'
        when:
            RestError error = restClient.getJobAsError(getSearchUrl(requestUrl))
        then:
            error.with {
                message == 'Not Found'
                template == 'No job by url specified'
                status == 404
                path == '/job/search'
            }
    }

    def 'should return conflict on dublicate url'() {
        String requestUrl = 'www.twitter.com'
        when:
            restClient.createJob(requestUrl)
        and:
            RestError error = restClient.createJobWithError(requestUrl)
        then:
            error.with {
                template == 'This url exists already'
                message == 'Conflict'
                status == 409
                path == '/job'
            }
    }

    def 'should return validation error on bad repository key'() {
        String requestUrl = 'www.twitter.com'
        when:
            RestError error = restClient.finishJobWithError(getBadFinishUrl(requestUrl))
        then:
            error.with {
                template == 'Wrong format of repository key'
                message == 'Bad Request'
                status == 400
                path == '/job/aaa'
            }
    }

    private String getDomain() {
        "http://localhost:$port"
    }

    private String getSearchUrl(String url) {
        "$domain/job/search?url=$url"
    }

    private String getBadFinishUrl(String url) {
        "$domain/job/aaa?url=$url"
    }
}
