package lt.skankjo.lunar.backendhomework.integration

import lt.skankjo.lunar.backendhomework.rest.exceptions.RestError
import lt.skankjo.lunar.backendhomework.rest.resources.job.JobPutRequest
import lt.skankjo.lunar.backendhomework.rest.resources.job.JobRequest
import lt.skankjo.lunar.backendhomework.rest.resources.job.JobResponse
import lt.skankjo.lunar.backendhomework.rest.resources.job.UnfinishedJobsResponse
import lt.skankjo.lunar.backendhomework.rest.resources.root.Root
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpEntity
import org.springframework.http.HttpMethod
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component

@Component
class RestClient {

    private final TestRestTemplate template

    @Autowired
    RestClient(TestRestTemplate template) {
        this.template = template
    }

    Root getRoot() {
        template.getForObject('/', Root)
    }

    String getJobsUrl() {
        root.getLink('jobs').href
    }

    JobResponse getJob(String selfUrl) {
        template.getForObject(selfUrl, JobResponse)
    }

    RestError getJobAsError(String selfUrl) {
        template.getForObject(selfUrl, RestError)
    }

    String getJobEditUrl(String selfUrl) {
        getJob(selfUrl).getLink('edit').href
    }

    String getJobFinishUrl(String selfUrl) {
        getJob(selfUrl).getLink('finish').href
    }

    JobResponse createJob(JobRequest request) {
        template.postForObject(jobsUrl, request, JobResponse)
    }

    JobResponse createJob(String url) {
        createJob(new JobRequest(url: url))
    }

    RestError createJobWithError(String url) {
        template.postForObject(jobsUrl, new JobRequest(url: url), RestError)
    }

    JobResponse updateJob(String selfUrl, JobPutRequest request) {
        String url = getJobEditUrl(selfUrl)
        put(url, JobResponse, request)
    }

    JobResponse updateJob(String selfUrl, List<String> famousPeople) {
        JobPutRequest request = new JobPutRequest(famousPeople: famousPeople)
        updateJob(selfUrl, request)
    }

    JobResponse finishJob(String selfUrl) {
        String url = getJobFinishUrl(selfUrl)
        put(url, JobResponse)
    }

    RestError finishJobWithError(String finishUrl) {
        put(finishUrl, RestError)
    }

    UnfinishedJobsResponse getUnfinishedJobs() {
        template.getForObject(jobsUrl, UnfinishedJobsResponse)
    }

    private <T> T put(url, T responseType) {
        put(url, responseType, HttpEntity.EMPTY)
    }

    private <T> T put(url, T responseType, Object request) {
        put(url, responseType, new HttpEntity<Object>(request))
    }

    private <T> T put(url, T responseType, HttpEntity<Object> httpEntity) {
        ResponseEntity<T> response = template.exchange(url, HttpMethod.PUT, httpEntity, responseType)
        response.body
    }

}
